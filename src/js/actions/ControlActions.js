import PresentationDispatcher from "dispatcher/PresentationDispatcher"
import PresentationConstants from "constants/PresentationConstants"

export default {

  previousSlide() {
    PresentationDispatcher.dispatch({
      type: PresentationConstants.ActionTypes.PREVIOUS_SLIDE
    });
  },

  nextSlide() {
    PresentationDispatcher.dispatch({
      type: PresentationConstants.ActionTypes.NEXT_SLIDE
    });
  },

  toggleContrast() {
    PresentationDispatcher.dispatch({
      type: PresentationConstants.ActionTypes.TOGGLE_CONTRAST
    });
  },

  toggleHighlightComponents() {
    PresentationDispatcher.dispatch({
      type: PresentationConstants.ActionTypes.TOGGLE_HIGHLIGHT_COMPONENTS
    });
  }

};