import React from "react";
import BulletListSlide from "components/slide-types/BulletListSlide";

export default React.createClass({

  displayName: "Always re-render",

  render() {
    return (
      <BulletListSlide title="Always re-render" {...this.props}>
        <p>Components and their children 'always' re-render</p>
        <p>Simple compared to Backbone confusion</p>
      </BulletListSlide>
    );
  }
});