import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `it("should call the event handler", function() {
  var node = document.findElementById("id");
  React.addons.TestUtils.Simulate.click(node);
  /* assertions, etc */
});`;

export default React.createClass({

  displayName: "Events - Simulation",

  render() {
    return (
      <ContentSlide title="Simulating events" {...this.props}>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});