import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  _onClick(e) {
    alert("Hello!");
  },
  render() {
    return <a onClick={this._onClick}>Click here</a>;
  }
});`;

export default React.createClass({

  displayName: "Auto event delegation",

  render() {
    return (
      <ContentSlide title="Auto event delegation" {...this.props}>
        <p>Event listeners automatically set and removed</p>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});