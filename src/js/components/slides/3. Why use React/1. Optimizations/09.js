import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";

export default React.createClass({

  displayName: "Event optimizations",

  render() {
    return (
      <ContentSlide title="Event optimizations" {...this.props} />
    );
  }
});