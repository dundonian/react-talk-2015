import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";

export default React.createClass({

  displayName: "Flux - Actions",

  render() {
    return (
      <ContentSlide title="Actions" {...this.props}>
        <img src="/img/flux-simple-f8-diagram-1300w.png" width="650" />
        <p>Triggered from UI</p>
        <p>Triggers events on the Dispatcher</p>
      </ContentSlide>
    );
  }
});