import ContentSlide from "components/slide-types/ContentSlide";
import MajorSections from "components/MajorSections";
import React from "react";

export default React.createClass({

  displayName: "Contents",

  render() {
    return (
      <ContentSlide title="All about React" {...this.props}>
        <MajorSections selected="Flux" />
      </ContentSlide>
    );
  }
});