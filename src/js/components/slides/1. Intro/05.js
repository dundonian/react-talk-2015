import ContentSlide from "components/slide-types/ContentSlide";
import React from "react";

export default React.createClass({

  displayName: "Javascript History",

  render() {
    return (
      <ContentSlide title="Javascript history" {...this.props}>
        <h3>Frameworks</h3>
        <ul>
          <li>2009: Angular</li>
          <li>2011: Ember</li>
        </ul>
      </ContentSlide>
    );
  }
});