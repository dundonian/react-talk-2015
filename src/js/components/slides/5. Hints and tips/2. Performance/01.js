import ContentSlide from "components/slide-types/ContentSlide";
import MajorSections from "components/MajorSections";
import React from "react";

export default React.createClass({

  displayName: "Performance",

  render() {
    return (
      <ContentSlide title="Performance" {...this.props}>
        <p>React Performance Tools</p>
        <a href="https://facebook.github.io/react/docs/perf.html">https://facebook.github.io/react/docs/perf.html</a>
      </ContentSlide>
    );
  }
});