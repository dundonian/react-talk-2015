var files = require.context("./slides", true, /.js$/);

// Require all of the slides
var slides = files.keys().map(key => files(key));

module.exports = slides;