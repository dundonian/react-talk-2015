import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";

export default React.createClass({

  displayName: "BulletListSlide",

  _getListItems() {
    return React.Children.map(this.props.children, child => <li>{child.props.children}</li>)
  },

  render() {
    var listItems = this._getListItems();
    return (
      <ContentSlide {...this.props}>
        <ul>
          {listItems}
        </ul>
      </ContentSlide>
    );
  }
});