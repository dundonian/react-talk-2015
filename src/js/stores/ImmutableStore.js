var Immutable = require("immutable");
var EventEmitter = require('events').EventEmitter;

class ImmutableStore extends EventEmitter {

  constructor() {
    super();
    this.setInitialData();
    this.registerListeners();
  }

  addChangeListener(callback) {
    this.on("change", callback);
  }

  removeChangeListener(callback) {
    this.removeListener("change", callback);
  }

  setInitialData() {
    this._data = Immutable.Map(this.getInitialData());
  }

  get(key) {
    return this._data.get(key);
  }

  getAll() {
    return this._data.toJS();
  }

  has(key) {
    return this._data.has(key);
  }

  set(key, value) {
    var data = key;

    if (value !== undefined) {
      data = {};
      data[key] = value;
    }

    this.doSet(data);
  }

  doSet(data) {
    var changeset = {};
    var hasChange = false;

    Object.keys(data).forEach(key => {

      var oldValue = this.get(key),
        value = data[key];

      var newData = this._data.set(key, value);
      if (this._data !== newData) {
        hasChange = true;
        this._data = newData;
        this.emit("change:" + key, value, oldValue);
        changeset[key] = value;
      }
    }, this);

    if (hasChange) {
      this.emit("change", changeset);
    }
  }

  registerListeners() {
  }

  getInitialData() {
    return {};
  }

  unset(key) {
    if (this.has(key)) {
      var oldValue = this.get(key);
      this._data = this._data.delete(key);
      this.emit("change:" + key, undefined, oldValue);
      var changeSet = {};
      changeSet[key] = undefined;
      this.emit("change", changeSet);
    }
  }

  clear() {
    this._data.keySeq().forEach(key => this.unset(key));
  }

  reset() {
    this._setInitialData();
  }
}

module.exports = ImmutableStore;