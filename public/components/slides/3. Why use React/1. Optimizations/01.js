import ContentSlide from "components/slide-types/ContentSlide";
import MajorSections from "components/MajorSections";
import React from "react";

export default React.createClass({

  displayName: "Contents",

  render() {
    return (
      <ContentSlide {...this.props}>
        <MajorSections selected="Why use React?" />
      </ContentSlide>
    );
  }
});