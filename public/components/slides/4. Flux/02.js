import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";

export default React.createClass({

  displayName: "Flux",

  render() {
    return (
      <ContentSlide title="Flux" {...this.props}>
        <p>A simple, top-down architecture specification</p>
        <img src="/img/flux-simple-f8-diagram-1300w.png" width="650" />
        <p><a href="https://facebook.github.io/flux/docs/overview.html">Image credit Facebook. More on Flux here</a></p>
      </ContentSlide>
    );
  }
});