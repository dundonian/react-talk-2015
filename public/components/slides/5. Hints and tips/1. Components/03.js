import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `var Split = React.createClass({
  render() {
    var children = React.Children.map(this.props.children, child => <div>{child}</div>);

    return <div className="split">{children}</div>;
  }
});`;

export default React.createClass({

  displayName: "Composing Components",

  render() {
    return (
      <ContentSlide title="Composing Components" {...this.props}>
        <p>Meta demo</p>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});