import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import Split from "components/Split";

export default React.createClass({

  displayName: "Composing Components",

  render() {
    return (
      <ContentSlide title="Composing Components" {...this.props}>
        <p>Meta demo</p>
        <Split>
          <p>left</p>
          <p>right</p>
        </Split>
      </ContentSlide>
    );
  }
});