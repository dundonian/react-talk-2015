import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `render() {
  <Split>
    <p>left</p>
    <p>right</p>
  </Split>
}`;

export default React.createClass({

  displayName: "Composing Components",

  render() {
    return (
      <ContentSlide title="Composing Components" {...this.props}>
        <p>Meta demo</p>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});