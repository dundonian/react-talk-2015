import ContentSlide from "components/slide-types/ContentSlide";
import MajorSections from "components/MajorSections";
import React from "react";

export default React.createClass({

  displayName: "Performance",

  render() {
    return (
      <ContentSlide title="React Performance Tools" {...this.props}>
        <ol>
          <li>Perf.start()</li>
          <li>Perform an action</li>
          <li>Perf.stop()</li>
          <li>Profit!</li>
        </ol>
      </ContentSlide>
    );
  }
});