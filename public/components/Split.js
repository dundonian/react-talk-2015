import React from "react";

export default React.createClass({

  displayName: "Split",

  render() {

    var children = React.Children.map(this.props.children, child => <div>{child}</div>);

    return (
      <div className="split">{children}</div>
    );
  }
});