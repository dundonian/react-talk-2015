import React from "react/addons";
import Header from "components/Header";
import Footer from "components/Footer";
import SVGs from "components/SVGs";
import PresentationStore from "stores/PresentationStore";
import Slides from "components/Slides";
import classnames from "classnames";

function getState() {
  return PresentationStore.getAll();
}

export default React.createClass({

  displayName: "Presentation",

  getInitialState() {
    return getState();
  },

  _onChange() {
    React.addons.Perf.start();
    this.setState(getState());
    React.addons.Perf.stop();
    React.addons.Perf.printInclusive();
  },

  componentDidMount() {
    PresentationStore.addChangeListener(this._onChange);
  },

  componentWillUnmount() {
    PresentationStore.removeChangeListener(this._onChange);
  },

  getCurrentSlide() {
    return Slides[this.state.slide - 1];
  },

  render() {
    var Slide = this.getCurrentSlide();
    var classes = classnames({
      "high-contrast": this.state.highContrast,
      "highlight-components": this.state.highlightComponents
    });

    return (
      <div className={classes}>
        <SVGs />
        <Header />
        <Slide />
        <Footer slide={this.state.slide} slideCount={this.state.slideCount} />
      </div>
    );
  }
});