import React from "react";
import classnames from "classnames";

export default React.createClass({

  displayName: "Selectable List",

  getDefaultProps() {
    return {
      sections: [],
      selected: ""
    }
  },

  render() {

    var lis = this.props.sections.map(s => {
      return <li className={(s === this.props.selected) ? "selected" : ""}>{s}</li>;
    });

    var classes = classnames(this.props.className, "center", "sections", "highlightable-component");

    return (
      <ul className={classes}>{lis}</ul>
    );
  }
});